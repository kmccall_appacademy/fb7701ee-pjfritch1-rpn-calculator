class RPNCalculator
  def initialize
    @stack = Array.new
  end

  attr_accessor :stack


  def push(num)
    @stack << num.to_f
  end

  def value
    @stack[-1]
  end

  def length_checker
    if @stack.length < 2
      raise "calculator is empty"
    end
  end


  def plus
    length_checker
      a = @stack.pop
      b = @stack.pop
      @stack << a + b
  end

  def minus
    length_checker
      a = @stack.pop
      b = @stack.pop
      @stack << b - a
  end

  def times
    length_checker
      a = @stack.pop
      b = @stack.pop
      @stack << a * b
  end

  def divide
    length_checker
      a = @stack.pop
      b = @stack.pop
      @stack << b / a
  end

  def tokens(string)
    final_tokens = []
    tokns = string.split
    tokns.each do |t|
      if t.to_i.to_s == t
        final_tokens << t.to_i
      else
        final_tokens << t.to_sym
      end
    end

    final_tokens
  end

  def evaluate(string)
    tokes = self.tokens(string)
    tokes.each do |t|
      if t.is_a?(Integer)
        @stack.push(t.to_f)
      elsif t.is_a?(Symbol)
        case t
        when :+
          self.plus
        when :-
          self.minus
        when :*
          self.times
        when :/
          self.divide
        end
      end
    end
    self.value
  end
end

# Third time's the charm?
